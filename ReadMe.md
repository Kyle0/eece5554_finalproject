### ZED Mini for SLAM
This repository contains code and datasets used for Group 19's EECE 5554 final
project. This is also intended to provide general information on collecting and
working with ZED Mini data. Note, this is NOT a ROS package, but does contain
some launch files for data visualization.

Group Members:
- Colin Keil
- Marcos Oliveira
- Andac Demir
- Yating Chen

The presentation can be viewed [here](https://docs.google.com/presentation/d/1eU0foseKnyYBD6XCYYP_9d4xDryoePscIzgQw0UJ1z8/edit?usp=sharing)

## Project Structure
The project consists of a number of datasets collected by the ZED Mini camera,
along with code to visualize and analyze them. We also test the ZED camera with
Vic's simple_vslam library.

## Requirements
In order to view the datasets in ros or ZED software, you need a computer with
a NVIDIA gpu and the correct version of CUDA. Detailed instructions for setting
up the ZED SDK and Ros Wrapper can be found in the [docs](docs/ZED_SDK_ROS_install_instructions.pdf)

Detailed information on the ROS wrapper can be found:
https://www.stereolabs.com/docs/ros/

The simple_vslam demo and the [loop closure analysis](scripts/CovarainceAnalysis.ipynb) can be run without a NVIDI GPU.

## Downloading datasets
Datasets can be downloaded from:
https://drive.google.com/drive/folders/12CW243ov-XnQfL-xT3PNmHe-NrfMLLw-?usp=sharing

Note, it is not necessary to download all the datasets. Only download the svo files
if you can install the ZED sdk. They cannot be viewed without the ZED software.

All datasets should be installed and where appropriate, unzipped in the [datasets](datasets) directory.

The [datasets](datasets) directory has more detailed information.

The simple_vslam datasets contain raw images and can be used without the ZED SDK.
These are also available in the above linked google drive folder, or [here](https://drive.google.com/open?id=1IhX4vvkCOZyTX99gQtxPM8jBa5CP3m80)

## Dataset Visualizations
See the [datasets readme](datasets/ReadMe.md) for detailed information on how to
view datasets. There are also guides on extracting images, converting data to
rosbag formats etc.

## Covariance Visualizations and Analysis
A nicely formatted Jupyter Notebook is available in the [scripts](scripts) folder,
with a somewhat detailed analysis of the ZED mini's loop closure failures in larger environments.
In general, it seems that the ZED mini is overly confident in it's pose accuracy
when loops are large. This can be viewed with figures in GitLab as well, without any need to rerun the code.

## simple_vslam
We used Vic's simple vslam package to test execution with an external vslam library. The results are not great, but the package does run. I have included a slightly tweaked copy of the specific commit that we used in the simple_vslam directory. The vslam package requires python 3. The environment variables conflic with ROS for cv2. Inn order to run the package you need to temporarily remove the ROS entries from "$PYTHONPATH". The easiest way to do this is to just delete the whole python path. You will also need to make sure that the [kitchen dataset](https://drive.google.com/open?id=1IhX4vvkCOZyTX99gQtxPM8jBa5CP3m80) is downloaded to the datasets folder. Execute the following commands to run the simple vslam code:
```
$ export PYTHONPATH=""
$ python3 vslam.py --config simple_vslam/config/zedm_720p.yaml
```

## Contributions
All members of the team contributed to the project. Only Colin had access to the camera, and had access to a computer that could run the ZED SDK. This created a bottleneck in the group workflow, only Colin could do lots of tasks, but we tried to minimize this issue. Also, most group members were using virtual machines and had significant trouble running SLAM Libraries. The contributions breakdown as follows:
- Marcos Oliveira: Experimented with simple_vslam, & other SLAM libraries, researched vslam/vio baground information for the presentation, built presentation slides
- Andac Demir: Worked on using datasets with external SLAM libraries, researched vslam/vio baground information for the presentation, built presentation slides
- Yating Chen: Worked on setting up orb SLAM (large time commitment), able to run simple_vslam demo, developed slides
- Colin Keil: Collected Datasets, Formatted and Extracted Data, Made Data Visualizations, Wrote analysis notebook, got the datasets to work with simple_vslam
